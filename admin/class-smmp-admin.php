<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://whitelabelcoders.com
 * @since      1.0.0
 *
 * @package    Smmp
 * @subpackage Smmp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Smmp
 * @subpackage Smmp/admin
 * @author     Wojciech <wzatorski@whitelabelcoders.com>
 */
class Smmp_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Smmp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Smmp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/smmp-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Smmp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Smmp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/smmp-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register the settings page for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function settings_page() {
		require_once plugin_dir_path( __FILE__ ) . 'partials/smmp-admin-settings.php';

		add_options_page( __('SalesManagoMP', 'smmp'), 'SMMP', 'manage_options', $this->plugin_name, 'Smmp_Settings::render_page' );
	}

	/**
	 * Register the settings page for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function register_settings() {
		require_once plugin_dir_path( __FILE__ ) . 'partials/smmp-admin-settings.php';
		register_setting( 'smmp_plugin_options', 'smmp_plugin_options', 'Smmp_Settings::options_validate' );
		add_settings_section( 'global_settings', __('Global Settings', 'smmp'), 'Smmp_Settings::smmp_plugin_section_text', 'smmp_plugin' );
		add_settings_section( 'newsletter_settings', __('Newsletter Main Page Settings', 'smmp'), 'Smmp_Settings::smmp_plugin_section_text_newsletter', 'smmp_newsletter' );
		add_settings_section( 'newsletter_page_settings', __('Newsletter List Page Settings', 'smmp'), 'Smmp_Settings::smmp_plugin_section_text_newsletter_page', 'smmp_newsletter_page' );
		add_settings_section( 'newsletter_mailing', __('Newsletter Mailing Settings', 'smmp'), 'Smmp_Settings::smmp_plugin_section_text_mailing_page', 'smmp_mailing_page' );
		add_settings_field( 'smmp_plugin_setting_list', __('Newsletter tags list (ex tag1,tag2,tag3)'), 'Smmp_Settings::smmp_plugin_setting_list', 'smmp_newsletter_page', 'newsletter_page_settings' );
		add_settings_field( 'smmp_plugin_setting_credentials', __('Owner'), 'Smmp_Settings::smmp_plugin_setting_owner', 'smmp_plugin', 'global_settings' );
		add_settings_field( 'smmp_plugin_setting_endpoint', __('Endpoint', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_endpoint', 'smmp_plugin', 'global_settings' );
		add_settings_field( 'smmp_plugin_setting_apikey', __('API key', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_apikey', 'smmp_plugin', 'global_settings' );
		add_settings_field( 'smmp_plugin_setting_apisecret', __('API secret', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_apisecret', 'smmp_plugin', 'global_settings' );
		add_settings_field( 'smmp_plugin_setting_clientid', __('Client ID', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_clientid', 'smmp_plugin', 'global_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_tag1', __('TAG #1', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_tag1', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_title1', __('Title #1', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_title1', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_image1', __('Image #1', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_image1', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_descr1', __('Description #1', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_descr1', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_priv1', __('For members #1', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_priv1', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_tag2', __('TAG #2', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_tag2', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_title2', __('Title #2', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_title2', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_image2', __('Image #2', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_image2', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_descr2', __('Description #2', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_descr2', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_newsletter_priv2', __('For members #2', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_newsletter_priv2', 'smmp_newsletter', 'newsletter_settings' );
		add_settings_field( 'smmp_plugin_setting_mail_text', __('Subscription mail', 'smmp'), 'Smmp_Settings::smmp_plugin_setting_mail_text', 'smmp_mailing_page', 'newsletter_mailing' );
	}

    public function validate_tags($res,$res2) {
	global $wpdb;
	global $wp_registered_widgets;

	$tags = explode(',', $res2['list']);
	$newstag = [];

	$query = "SELECT post_content FROM `wp_posts` WHERE `post_content` LIKE '%newsletterNG%'";
	$results = $wpdb->get_results($query, ARRAY_A);

	foreach ($results as $row) {
	    preg_match_all('@<!-- wp:shortcode -->(.+?)<!-- /wp:shortcode -->@is', $row['post_content'], $matches);
	    if (is_array($matches) && count($matches[1]) > 0) {
		foreach ($matches[1] as $text) {
		    if (strpos($text,'newsletterNG') > 0) {
		        preg_match_all('@([a-z]+)="(.+?)"@is', $text, $fields);
			if (is_array($fields) && count($fields)>1 && count($fields[0]) > 0) {
			    $t = [];
			    for ($i = 0; $i < count($fields[0]); $i++) {
				$tag = $fields[1][$i];
				$var = $fields[2][$i];
				$t[$tag] = $var;
			    }

			    if (in_array($t['tag'], $tags)) {
				$newstag[] = [$t['tag'], $t['title'], $t['description'], $t['priv'], $t['image']];
			    }
			}
		    }
		}
	    }
	}

	$sidebar = wp_get_sidebars_widgets();

	$settings = get_option('widget_widget_newsletter_sidebar');

	foreach ($sidebar as $n) {
	    foreach ($n as $item) {
		if (strpos($item, 'idget_newsletter_sidebar') > 0) {
		    $s = explode('-', $item);
		    $id = intval($s[count($s)-1]);
		    $wid = $settings[$id];
		    if (is_array($wid)) {
			if (in_array($wid['tag'], $tags)) {
			    $newstag[] = [$wid['tag'], $wid['title'], $wid['desc'], $wid['locked'], wp_get_attachment_image_url( $wid['image_id'], 'full', false )];
			}
		    }
		}
	    }
	}

	update_option('smmp_plugin_options_list', $newstag);

	return $newstag;

    }
}
