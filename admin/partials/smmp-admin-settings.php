<?php

/**
 * Settings window
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://whitelabelcoders.com
 * @since      1.0.0
 *
 * @package    Splitpayment
 * @subpackage Splitpayment/admin/partials
 */

class Smmp_Settings {
	public static function render_page() {
?>
    <h2><?php echo __('SMMP Settings', 'smmp') ?></h2>
    <form action="options.php" method="post">
        <?php 
        settings_fields( 'smmp_plugin_options' );
        do_settings_sections( 'smmp_plugin' );
        do_settings_sections( 'smmp_newsletter' );
        do_settings_sections( 'smmp_newsletter_page' );
        do_settings_sections( 'smmp_mailing_page' ); ?>
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( __('Save', 'smmp') ); ?>" />
    </form>

    <?php
	}

	public static function options_validate( $input ) {
	    $newinput['owner'] = trim( $input['owner'] );
	    if ( ! preg_match( '/^[a-z0-9:@\.]+$/i', $newinput['owner'] ) ) {
		$newinput['owner'] = '';
	    }

	    $newinput['apikey'] = trim( $input['apikey'] );
	    if ( ! preg_match( '/^[a-zA-Z0-9]+$/i', $newinput['apikey'] ) ) {
		$newinput['apikey'] = '';
	    }

	    $newinput['apisecret'] = trim( $input['apisecret'] );
	    if ( ! preg_match( '/^[a-zA-Z0-9\-\.]+$/i', $newinput['apisecret'] ) ) {
		$newinput['apisecret'] = '';
	    }

	    $newinput['endpoint'] = $input['endpoint'];
	    $newinput['clientid'] = $input['clientid'];
	    $newinput['tag1'] = $input['tag1'];
	    $newinput['tag2'] = $input['tag2'];
	    $newinput['title1'] = $input['title1'];
	    $newinput['title2'] = $input['title2'];
	    $newinput['image1'] = $input['image1'];
	    $newinput['image2'] = $input['image2'];
	    $newinput['priv1'] = $input['priv1'];
	    $newinput['priv2'] = $input['priv2'];
	    $newinput['descr1'] = $input['descr1'];
	    $newinput['descr2'] = $input['descr2'];
	    $newinput['list'] = $input['list'];
	    $newinput['mailtext'] = $input['mailtext'];

	    return $newinput;
	}

	public static function smmp_plugin_section_text() {
	    echo '<p>'.__('Here you can set all the options for using the SM API', 'smmp').'</p>';
	}

	public static function smmp_plugin_section_text_newsletter() {
	    echo '<p>'.__('Here you can set all the options for Newsletter on main page', 'smmp').'</p>';
	}

	public static function smmp_plugin_section_text_newsletter_page() {
	    echo '<p>'.__('Here you can set all the options for Newsletter on list page', 'smmp').'</p>';
	}

	public static function smmp_plugin_section_text_mailing_page() {
	    echo '<p>'.__('Here you can set all the options for mailing', 'smmp').'</p>';
	}

	public static function smmp_plugin_setting_list() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_list' name='smmp_plugin_options[list]' style='width: 300px;' type='text' value='" . esc_attr( $options['list'] ) . "' />";
	}

	public static function smmp_plugin_setting_owner() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_owner' name='smmp_plugin_options[owner]' type='text' value='" . esc_attr( $options['owner'] ) . "' />";
	}

	public static function smmp_plugin_setting_endpoint() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_endpoint' name='smmp_plugin_options[endpoint]' type='text' value='" . esc_attr( $options['endpoint'] ) . "' />";
	}

	public static function smmp_plugin_setting_apikey() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_apikey' name='smmp_plugin_options[apikey]' type='text' value='" . esc_attr( $options['apikey'] ) . "' />";
	}

	public static function smmp_plugin_setting_apisecret() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_apisecret' name='smmp_plugin_options[apisecret]' type='text' value='" . esc_attr( $options['apisecret'] ) . "' />";
	}

	public static function smmp_plugin_setting_clientid() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_clientid' name='smmp_plugin_options[clientid]' type='text' value='" . esc_attr( $options['clientid'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_tag1() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_tag1' name='smmp_plugin_options[tag1]' type='text' value='" . esc_attr( $options['tag1'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_tag2() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_tag2' name='smmp_plugin_options[tag2]' type='text' value='" . esc_attr( $options['tag2'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_title1() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_title1' name='smmp_plugin_options[title1]' type='text' value='" . esc_attr( $options['title1'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_title2() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_title2' name='smmp_plugin_options[title2]' type='text' value='" . esc_attr( $options['title2'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_image1() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_image1' name='smmp_plugin_options[image1]' type='text' value='" . esc_attr( $options['image1'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_image2() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_image2' name='smmp_plugin_options[image2]' type='text' value='" . esc_attr( $options['image2'] ) . "' />";
	}

	public static function smmp_plugin_setting_newsletter_priv1() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_priv1' name='smmp_plugin_options[priv1]' type='checkbox' value='1' " . ($options['priv1']==1?'checked="checked"':'') . "' />";
	}

	public static function smmp_plugin_setting_newsletter_priv2() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<input id='smmp_plugin_setting_newsletter_priv2' name='smmp_plugin_options[priv2]' type='checkbox' value='1' " . ($options['priv2']==1?'checked="checked"':'') . "' />";
	}

	public static function smmp_plugin_setting_newsletter_descr1() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<textarea id='smmp_plugin_setting_newsletter_descr1' name='smmp_plugin_options[descr1]' cols='60' rows='4' />" . esc_attr( $options['descr1'] ) . "</textarea>";
	}

	public static function smmp_plugin_setting_newsletter_descr2() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<textarea id='smmp_plugin_setting_newsletter_descr2' name='smmp_plugin_options[descr2]' cols='60' rows='4' />" . esc_attr( $options['descr2'] ) . "</textarea>";
	}

	public static function smmp_plugin_setting_mail_text() {
	    $options = get_option( 'smmp_plugin_options' );
	    echo "<textarea id='smmp_plugin_setting_mailtext' name='smmp_plugin_options[mailtext]' cols='120' rows='10' />" . esc_attr( $options['mailtext'] ) . "</textarea>";
	}

}
?>