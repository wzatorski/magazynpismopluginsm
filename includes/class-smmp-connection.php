<?php

/**
 * SM Connection
 *
 * @link       https://whitelabelcoders.com
 * @since      1.0.0
 *
 * @package    Smmp
 * @subpackage Smmp/includes
 */

/**
 * Connection
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Smmp
 * @subpackage Smmp/includes
 * @author     Wojciech <wzatorski@whitelabelcoders.com>
 */
class Smmp_Connection {

	public $endpoint = 'app3.salesmanago.pl/api';
	public $clientID = '09kw2mhyaixvxlyo';
	public $apiKey = '43435323';
	public $apiSecret = '7mys0t14x8ttkigziuhd3h5jwq1iwvzv';
	public $sha;

	public function __construct() {
	    $options = get_option( 'smmp_plugin_options' );
	    $this->endpoint = $options['endpoint'];
	    $this->clientID = $options['clientid'];
	    $this->apiKey = $options['apikey'];
	    $this->apiSecret = $options['apisecret'];
	    $this->sha = sha1($this->apiKey . $this->clientID . $this->apiSecret, false);
	}

	/**
	 * Send User Data
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function user($data, $async = true) {
	    $extra = [];
	    $extra['async'] = $async;
	    $extra['state'] = 'CUSTOMER';
	    $data = array_merge($data, $extra);
	    $args = array_merge($this->logIn(), $data);
	    $data = $this->call('/contact/upsert', $args, 'POST');

	    return $data;
	}

	/**
	 * Load User Data
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function loadUser($data) {
	    $args = array_merge($this->logIn(), $data);
	    $data = $this->call('/contact/list', $args, 'POST');

	    return $data;
	}

	/**
	 * Send Users Data
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function users($data, $owner = null, $async = true) {
	    $extra = [];
	    $extra['async'] = $async;
	    $extra['upsertDetails'] = $data;
	    $extra['owner'] = $owner;
	    $args = array_merge($this->logIn(), $extra);
	    $data = $this->call('/contact/batchupsertv2', $args, 'POST');

	    return $data;
	}

	/**
	 * Send Action Data
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function action($data) {
	    $args = array_merge($this->logIn(), $data);
	    $data = $this->call('/v2/contact/addContactExtEvent', $args, 'POST');

	    return $data;
	}

	/**
	 * Login Method
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function logIn() {
	    $data = [];
	    $data['clientId'] = $this->clientID;
	    $data['apiKey'] = $this->apiKey;
	    $data['requestTime'] = time();
	    $data['sha'] = $this->sha;

	    return $data;
	}

	/**
	 * API Command
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function apiReq($data) {
	}

	/**
	 * Curl connection to SalesManago
	 *
	 * Send GET/POST/PUT request to API
	 *
	 * @param array $path - URL.
	 * @param array $payload - Data.
	 * @param array $method - GET/POST.
	 * @since    1.0.0
	 */
	public function call( $path, $payload, $method ) {
		$headers = array(
			'Content-Type: application/json;charset=UTF-8',
		);

		$json_style = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
		$options    = array(
			CURLOPT_URL            => $this->endpoint . $path,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_TIMEOUT        => 10,
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_FAILONERROR    => false
		);

		if ( $method != 'GET' ) {
			$options[ CURLOPT_POST ]       = true;
			$options[ CURLOPT_POSTFIELDS ] = json_encode( $payload, $json_style );
		} else {
			$options[ CURLOPT_CUSTOMREQUEST ] = 'GET';
		}

		if ( 'PUT' === $method ) {
			$options[ CURLOPT_CUSTOMREQUEST ] = 'PUT';
		}

		$h = curl_init();
		curl_setopt_array( $h, $options );
		$ret = curl_exec( $h );
		curl_close( $h );

		$decoded = json_decode( $ret, true );

		if ( ! is_array( $decoded ) ) {
			$decoded = array();
		}

		return $decoded;
	}
}
