<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://whitelabelcoders.com
 * @since      1.0.0
 *
 * @package    Smmp
 * @subpackage Smmp/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Smmp
 * @subpackage Smmp/includes
 * @author     Wojciech <wzatorski@whitelabelcoders.com>
 */
class Smmp_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
