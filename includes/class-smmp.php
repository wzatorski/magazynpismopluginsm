<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://whitelabelcoders.com
 * @since      1.0.0
 *
 * @package    Smmp
 * @subpackage Smmp/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Smmp
 * @subpackage Smmp/includes
 * @author     Wojciech <wzatorski@whitelabelcoders.com>
 */
class Smmp {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Smmp_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'SMMP_VERSION' ) ) {
			$this->version = SMMP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'smmp';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Smmp_Loader. Orchestrates the hooks of the plugin.
	 * - Smmp_i18n. Defines internationalization functionality.
	 * - Smmp_Admin. Defines all hooks for the admin area.
	 * - Smmp_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-smmp-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-smmp-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-smmp-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-smmp-public.php';

		$this->loader = new Smmp_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Smmp_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Smmp_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Smmp_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'settings_page' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );
		$this->loader->add_action( 'added_option_smmp_plugin_options', $plugin_admin, 'validate_tags', 10, 2);
		$this->loader->add_action( 'update_option_smmp_plugin_options', $plugin_admin, 'validate_tags', 10, 3);
#		$this->loader->add_action( 'added_option', $plugin_admin, 'validate_tags', 10, 2);
#		$this->loader->add_action( 'update_option', $plugin_admin, 'validate_tags', 10, 3);
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Smmp_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_login', $plugin_public, 'login_action_to_sm', 10, 2 );
		$this->loader->add_action( 'wp_login', $plugin_public, 'wp_action_login', 20, 2 );
		$this->loader->add_action( 'wp_ajax_update_user_profile_data', $plugin_public, 'update_action_to_sm', 10);
		$this->loader->add_action( 'wp_payment_confirmation', $plugin_public, 'transaction_action_to_sm', 10, 3);
		$this->loader->add_action( 'wp_ajax_update_sm_clients', $plugin_public, 'update_sm_clients');
		$this->loader->add_action( 'wp_ajax_nopriv_update_sm_clients', $plugin_public, 'update_sm_clients');
		$this->loader->add_action( 'profile_update', $plugin_public, 'login_action_to_sm', 10, 3 );
		$this->loader->add_action( 'user_register', $plugin_public, 'register_action_to_sm', 10, 1 );
		$this->loader->add_action( 'wp_ajax_nopriv_get_playlist', $plugin_public, 'wp_action_player');
		$this->loader->add_action( 'wp_ajax_get_playlist', $plugin_public, 'wp_action_player');
		$this->loader->add_action( 'wp_ajax_nopriv_get_single_plan', $plugin_public, 'wp_action_basket');
		$this->loader->add_action( 'wp_ajax_get_single_plan', $plugin_public, 'wp_action_basket');
		$this->loader->add_action( 'wp_ajax_deactivate_subscription', $plugin_public, 'wp_action_cancel', 10);
		$this->loader->add_action( 'wp_ajax_admin_deactivate_subscription', $plugin_public, 'wp_action_cancel', 10);
		$this->loader->add_action( 'wp_ajax_charges_and_card_storing', $plugin_public, 'wp_action_change', 10);
		$this->loader->add_action( 'wp_ajax_nopriv_register_newsletter', $plugin_public, 'wp_action_newsletter', 10);
		$this->loader->add_action( 'wp_ajax_register_newsletter', $plugin_public, 'wp_action_newsletter', 10);
		$this->loader->add_action( 'wp_ajax_nopriv_generate_plans', $plugin_public, 'generate_plans');
		$this->loader->add_action( 'wp_ajax_generate_plans', $plugin_public, 'generate_plans');
		$this->loader->add_action( 'wp_ajax_nopriv_validate_plans', $plugin_public, 'validate_plans');
		$this->loader->add_action( 'wp_ajax_validate_plans', $plugin_public, 'validate_plans');
		$this->loader->add_action( 'wp_ajax_nopriv_subscribe_newsletter', $plugin_public, 'subscribe_newsletter');
		$this->loader->add_action( 'wp_ajax_subscribe_newsletter', $plugin_public, 'subscribe_newsletter');
		$this->loader->add_action( 'parse_request', $plugin_public, 'parse_sm_request');
		$this->loader->add_action( 'salesmenago_sharing_link_public_use_action', $plugin_public, 'share_links', 10, 1);
		$this->loader->add_action( 'wp_ajax_mit_cancel_user_chain', $plugin_public, 'wp_action_cancel2', 20);
		$this->loader->add_action( 'wp_ajax_share_article', $plugin_public, 'wp_action_share', 20);
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Smmp_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
