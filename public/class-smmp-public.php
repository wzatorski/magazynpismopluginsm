<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://whitelabelcoders.com
 * @since      1.0.0
 *
 * @package    Smmp
 * @subpackage Smmp/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Smmp
 * @subpackage Smmp/public
 * @author     Wojciech <wzatorski@whitelabelcoders.com>
 */
class Smmp_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The owner of clients
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $owner    The current owner of members.
	 */
	private $owner;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$options = get_option( 'smmp_plugin_options' );
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->owner = $options['owner'];
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Smmp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Smmp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/smmp-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Smmp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Smmp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/smmp-public.js', array( 'jquery' ), $this->version, false );
	}

	/**
	 * Update user
	 *
	 * @since    1.0.0
	 */
	public function login_action_to_sm($user_login, WP_User $user) {
	    global $wpdb;

	    $contactId = '';
	    $conn = new Smmp_Connection();
	    $data = [];
	    $data['contact'] = [];
	    $data['contact']['email'] = $user->user_email;
	    $data['contact']['externalId'] = $user->ID;
	    $data['owner'] = $this->owner;

	    $dbdata = $wpdb->get_row( $wpdb->prepare( "SELECT DISTINCT(user_email) AS user_email,wu.*,sd.*, pm.meta_value as plan_id, kof.plan_group_name FROM wp_users AS wu
                LEFT JOIN serviceuser_data AS sd ON (sd.user_id = wu.ID)
                LEFT JOIN discount_coupon_user AS dcu ON (dcu.recipient_id = wu.ID)
                LEFT JOIN wp_postmeta AS pm ON (pm.post_id = dcu.subscription_id AND pm.meta_key = 'espago_plan_id')
		LEFT JOIN wp_postmeta AS ps ON  (ps.post_id = sd.last_bought_plan_wp_id AND ps.meta_key = 'plan_group_id')
		LEFT JOIN kinds_of_plans AS kof ON (ps.meta_value = kof.id)

		WHERE wu.ID = %d LIMIT 1", $user->ID ), ARRAY_A);

	    if (is_array($dbdata) && strlen($dbdata['address_lastname']) > 0) {
		$data['contact']['name'] = $dbdata['address_lastname'] . ' ' . $dbdata['address_firstname'];
	        $data['contact']['phone'] = $dbdata['address_phone'];
		$data['contact']['company'] = $dbdata['address_company'];
		if (strlen($dbdata['address_apartamentnumber']) > 0) {
	    	    $data['contact']['address']['streetAddress'] = $dbdata['address_street'] . ' ' . $dbdata['address_streetnumber'] . '/' . $dbdata['address_apartamentnumber'];
		} else {
	    	    $data['contact']['address']['streetAddress'] = $dbdata['address_street'] . ' ' . $dbdata['address_streetnumber'];
		}

		$data['contact']['address']['zipCode'] = $dbdata['address_postcode'];
	        $data['contact']['address']['city'] = $dbdata['address_city'];
	    }

	    if (is_array($dbdata)) {
		$details = [];
		$dictionary = [];

		if (strlen($dbdata["actual_subscription_type"]))
		    $details['actual_subscription_type'] = $this->fix($dbdata["actual_subscription_type"]);
		if (strlen($dbdata["last_access_type"]))
		    $details['last_access_type'] = $this->fix($dbdata["last_access_type"]);
		if (strlen($dbdata["user_registered"])) {
		    $details['date.data_rejestracji'] = $this->fix($dbdata["user_registered"]);
		    $dictionary[] = ['name' => 'date.data_rejestracji', 'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["user_registered"])) * 1000];
		}
		if (strlen($dbdata["subscription_expires_gmt_date"]))
		    $details['date.subscription_expires_gmt_date'] = $this->fix($dbdata["subscription_expires_gmt_date"]);
		if (strlen($dbdata["paper_subscription_expires"])) {
		    $details['date.waznosc_prenumeraty'] = $this->fix($dbdata["paper_subscription_expires"]);
		    $dictionary[] = ['name' => 'date.waznosc_prenumeraty', 'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["paper_subscription_expires"])) * 1000];
		    $t1 = strtotime($this->fix($dbdata["subscription_expires_gmt_date"]));
		    $t2 = time();
		    $pren = 0;
		    if ($t1 > $t2)
			$pren = 1;

		    $dictionary[] = ['name' => 'prenumerata', 'type' => 'NUMBER', 'value' => $pren];
		}
		if (strlen($dbdata["subscription_expires_gmt_date"])) {
		    $details['date.waznosc.konta'] = $this->fix($dbdata["subscription_expires_gmt_date"]);
		    $dictionary[] = ['name' => 'date.subscription_expires_gmt_date', 'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["subscription_expires_gmt_date"])) * 1000];
		    $dictionary[] = ['name' => 'date.waznosc.konta', 'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["subscription_expires_gmt_date"])) * 1000];
		}
		if (strlen($dbdata["espago_cli"]))
		    $details['espago_cli'] = $this->fix($dbdata["espago_cli"]);
		if (strlen($dbdata["invoice_cb"]))
		    $details['Faktura'] = $this->fix($dbdata["invoice_cb"]);
		if (strlen($dbdata["address_apartamentnumber"]))
		    $details['Numer mieszkania'] = $this->fix($dbdata["address_apartamentnumber"]);
		if (strlen($dbdata["address_streetnumber"]))
		    $details['Numer ulicy'] = $this->fix($dbdata["address_streetnumber"]);

		$data['properties'] = $details;
		$data['dictionaryProperties'] = $dictionary;
	    }

	    $result = $conn->user($data);

	    if (is_array($result)) {
		$contactId = $result['contactId'];
	    }

	    setcookie('smclient', $contactId);
	}

	/**
	 * Update user by transaction
	 *
	 * @since    1.0.0
	 */
	public function update_transaction_action_to_sm($transactionID) {
	    global $wpdb;

	    $transaction = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM serviceuser_subscriptions_history
		LEFT JOIN wp_postmeta AS pm ON (pm.meta_value = plan_id AND pm.meta_key = 'espago_plan_id')
		LEFT JOIN wp_posts ON (post_type = 'subscription_plan' AND wp_posts.ID = pm.post_id)
		WHERE serviceuser_subscriptions_history.id = %d LIMIT 1", $transactionID ), ARRAY_A);

	    $user = get_user_by('id', $transaction['user_id']);

	    if ( ! ( $user instanceof WP_User ) ) {
		return;
	    }

	    $dbdata = $wpdb->get_row( $wpdb->prepare( "SELECT DISTINCT(user_email) AS user_email,wu.*,sd.*, pm.meta_value as plan_id, kof.plan_group_name FROM wp_users AS wu
                JOIN serviceuser_data AS sd ON (sd.user_id = wu.ID)
                LEFT JOIN discount_coupon_user AS dcu ON (dcu.recipient_id = wu.ID)
                LEFT JOIN wp_postmeta AS pm ON  (pm.post_id = dcu.subscription_id 	AND pm.meta_key = 'espago_plan_id')
		LEFT JOIN wp_postmeta AS ps ON  (ps.post_id = sd.last_bought_plan_wp_id AND ps.meta_key = 'plan_group_id')
		LEFT JOIN kinds_of_plans AS kof ON (ps.meta_value = kof.id)
		WHERE wu.ID = %d LIMIT 1", $user->ID ), ARRAY_A);

	    $contactId = null;
	    $conn = new Smmp_Connection();
	    $data = [];
	    $data['contact'] = [];
	    $data['contact']['email'] = $user->user_email;
	    $data['contact']['externalId'] = $user->ID;
	    $data['owner'] = $this->owner;

	    if (strlen($dbdata['address_lastname']) > 0) {
		$data['contact']['name'] = $dbdata['address_lastname'] . ' ' . $dbdata['address_firstname'];
	        $data['contact']['phone'] = $dbdata['address_phone'];
		$data['contact']['company'] = $dbdata['address_company'];
		if (strlen($dbdata['address_apartamentnumber']) > 0) {
	    	    $data['contact']['address']['streetAddress'] = $dbdata['address_street'] . ' ' . $dbdata['address_streetnumber'] . '/' . $dbdata['address_apartamentnumber'];
		} else {
	    	    $data['contact']['address']['streetAddress'] = $dbdata['address_street'] . ' ' . $dbdata['address_streetnumber'];
		}

		$data['contact']['address']['zipCode'] = $dbdata['address_postcode'];
	        $data['contact']['address']['city'] = $dbdata['address_city'];
	    }

	    $details = [];
	    $details['actual_subscription_type'] = $this->fix($dbdata["actual_subscription_type"]);
	    $details['last_access_type'] = $this->fix($dbdata["last_access_type"]);
	    $details['date.data_rejestracji'] = $this->fix($dbdata["user_registered"]);
	    $details['date.subscription_expires_gmt_date'] = $this->fix($dbdata["subscription_expires_gmt_date"]);
	    $details['date.waznosc_prenumeraty'] = $this->fix($dbdata["paper_subscription_expires"]);
	    $details['date.waznosc.konta'] = $this->fix($dbdata["subscription_expires_gmt_date"]);
	    $details['espago_cli'] = $this->fix($dbdata["espago_cli"]);
	    $details['Faktura'] = $this->fix($dbdata["invoice_cb"]);
	    $details['Plan'] = $this->fix($dbdata["plan_group_name"]);
	    $details['Plan szczegółowy'] = $this->fix($transaction["post_excerpt"]);
	    $details['Numer mieszkania'] = $this->fix($dbdata["address_apartamentnumber"]);
	    $details['Numer ulicy'] = $this->fix($dbdata["address_streetnumber"]);
	    $data['properties'] = $details;

	    $dictionary = [];
	    $dictionary[] = ['name' => 'date.data_rejestracji', 		'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["user_registered"])) * 1000];
	    $dictionary[] = ['name' => 'date.waznosc_prenumeraty',		'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["paper_subscription_expires"])) * 1000];
	    $dictionary[] = ['name' => 'date.subscription_expires_gmt_date', 	'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["subscription_expires_gmt_date"])) * 1000];
	    $dictionary[] = ['name' => 'date.waznosc.konta', 			'type' => 'DATE', 'value' => strtotime($this->fix($dbdata["subscription_expires_gmt_date"])) * 1000];


	    $t1 = strtotime($this->fix($dbdata["paper_subscription_expires"]));
	    $t2 = time();
	    $pren = 0;
	    if ($t1 > $t2)
		$pren = 1;

	    $dictionary[] = ['name' => 'prenumerata', 'type' => 'NUMBER', 'value' => $pren];

	    $data['dictionaryProperties'] = $dictionary;

	    $result = $conn->user($data);

	    if (is_array($result)) {
		$contactId = $result['contactId'];
	    }

	    return $contactId;
	}

	/**
	 * Update user
	 *
	 * @since    1.0.0
	 */
	public function update_action_to_sm($userid = null) {

	    if (!is_user_logged_in()) {
    		return;
	    }

	    if (!$userid)
		$user = wp_get_current_user();
	    else
		$user = get_user_by('id', $userid);

	    if ( ! ( $user instanceof WP_User ) ) {
		return;
	    }

	    $form_data = wp_parse_args($_GET['form_data']);
	    $args = wp_parse_args(
    		$form_data,
    		array(
        	    'address' => [],
    		)
	    );

	    $contactId = '';
	    $conn = new Smmp_Connection();
	    $data = [];
	    $data['contact'] = [];
	    $data['contact']['email'] = $user->user_email;
	    $data['contact']['externalId'] = $user->ID;
	    $data['owner'] = $this->owner;

	    if (is_array($args) && array_key_exists('address', $args)) {
		$data['contact']['name'] = $args['address']['lastname'] . ' ' . $args['address']['firstname'];
	        $data['contact']['phone'] = $args['address']['phone'];
		$data['contact']['company'] = $args['address']['company'];
		if (array_key_exists('apartamentnumber', $args['address']) && strlen($args['address']['apartamentnumber']) > 0) {
	    	    $data['contact']['address']['streetAddress'] = $args['address']['street'] . ' ' . $args['address']['streetnumber'] . '/' . $args['address']['apartamentnumber'];
		} else {
	    	    $data['contact']['address']['streetAddress'] = $args['address']['street'] . ' ' . $args['address']['streetnumber'];
		}
		$data['contact']['address']['zipCode'] = $args['address']['postcode'];
	        $data['contact']['address']['city'] = $args['address']['city'];
	    }

	    $result = $conn->user($data);

	    if (is_array($result)) {
		$contactId = $result['contactId'];
	    }

	    setcookie('smclient', $contactId);
	}

	public function wp_action_login($user_login, WP_User $user) {
	    $this->wp_action_to_sm($user, 'LOGIN');
	}

	public function wp_action_register($userid) {
	    $this->update_action_to_sm($userid);
	    $user = get_user_by('id', $userid);
	    $this->wp_action_to_sm($user, 'ACTIVATION', 'REJESTRACJA');
	}

	public function wp_action_post($post) {
	    $descr = '';
    	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user) {

		if (strpos($post->guid,'.pdf') > 0) {
		    $descr = 'Pobranie PDF';
		}

		if (strpos($post->guid,'.epub') > 0) {
		    $descr = 'Pobranie EPUB';
		}

		if (strpos($post->guid,'.mobi') > 0) {
		    $descr = 'Pobranie MOBI';
		}

		if (strpos($post->guid,'.jpg') > 0) {
	    	    $descr = 'Pobranie OKŁADKI';
		}

		$this->wp_action_to_sm($user, 'DOWNLOAD', 'MagazynPismo', $descr);
	    }
	}

	public function wp_action_player() {
    	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user)
		$this->wp_action_to_sm($user, 'VISIT', 'Player', 'Uruchomiony player');
	}

	public function wp_action_basket() {
	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user)
		$this->wp_action_to_sm($user, 'VISIT', 'Magazyn Koszyk', 'Produkt w koszyku');
	}

	public function wp_action_cancel() {
	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user)
		$this->wp_action_to_sm($user, 'VISIT', 'Magazyn Mój Profil', 'REZYGNACJA');
	}

	public function wp_action_change() {
	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user)
		$this->wp_action_to_sm($user, 'VISIT', 'Magazyn Mój Profil', 'Zmiana karty');
	}

	public function wp_action_cancel2() {
	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user)
		$this->wp_action_to_sm($user, 'CANCEL', 'Rezygnacja');
	}

	public function share_links($args) {
	    $tags = ['MGM_ODBIORCA'];
	    $res = $this->wp_action_to_sm($args['recipient_email'], 'VISIT', 'Podziel się treścią', 'Podziel się treścią - aktywacja linka', $tags);
	}

	public function wp_action_share() {
	    $user = wp_get_current_user();
	    if (is_user_logged_in() && $user)
		$this->wp_action_to_sm($user, 'MGM_SHARE', 'Podziel się artykułem');

	        $args = wp_parse_args(
    		    $_GET,
		        array(
    			'email' => '',
    		        'article_id' => '',
    			'token' => '',
    		        )
		);

	    if (is_user_logged_in() && strlen($args['email']))
		$this->wp_action_to_sm($args['email'], 'MGM_RECEIVER', 'Podziel się artykułem');
	}

	public function wp_action_newsletter() {
	    global $mail_alternatives;
	    $tag = $_REQUEST['tag']; 
	    $email = $_REQUEST['email'];
	    $usercode = isset($_REQUEST['usercode'])?$_REQUEST['usercode']:null;
	    $owner = isset($_REQUEST['owner'])?$_REQUEST['owner']:null;
	    $user = wp_get_current_user();
	    $cookie = $_COOKIE['newsletter_sign'];

	    $hash = base64_encode(sha1(md5($tag).'MAILING'.md5($email)));

	    if ($cookie) {
		$cookie = $cookie * 1;
		if (time() < ($cookie + (60 * 15))) {
		    echo json_encode(['result' => -1, 'save' => 0]);die();
		}
	    }

	    if (strlen($tag) && strlen($email)) {

		if (!is_user_logged_in()) {
		    $user = $email;
		}

		$conn = new Smmp_Connection();
		$data = [];
		$data['owner']= $this->owner;
		$data['email']= [ $email ];
		$usrd = $conn->loadUser($data);
		$tags = [$tag];
		$options = get_option( 'smmp_plugin_options' );
		$content = $options['mailtext'];

		if ($usercode && strlen($usercode) > 2) {
		    $contactDetail = [];
		    $contactDetail['mgm_data'] = $usercode;
		    $contactDetail['owner'] = $owner;
		    send_gift_proposition_email($user->ID, $email, $usercode);
		} else {
		    $contactDetail = null;
		}

		if ( $usrd && is_array($usrd) && array_key_exists('contacts', $usrd) && count($usrd['contacts']) > 0 ) {

		    $contact = $usrd['contacts'][0];
		    $utags = $contact['contactTags'];

		    $found = 0;
		    foreach ($utags as $utag) {
			if (strcmp(strtolower($utag['tagName']), strtolower($tag)) == 0) {
			    $found++;
			}
		    }

		    if ($found > 0) {
			$res = $this->wp_action_to_sm($user, 'VISIT', 'Newsletter', 'Subskrypcja newslettera', NULL, $tags, $contactDetail);
			echo json_encode(['result' => $res, 'save' => 0]);
			die();
		    } else {
			$res = $this->wp_action_to_sm($user, 'VISIT', 'Newsletter', 'Subskrypcja newslettera', $tags, NULL, $contactDetail);
			echo json_encode(['result' => $res, 'save' => 1]);
			if (!is_user_logged_in()) {
			    $headers = [];
			    $subject = '[Magazyn Pismo] Subskrypcja newslettera';
			    $headers[] = 'From: ' . get_bloginfo('name') . ' <noreply@' . preg_replace('@[a-z:]+//@', '', get_bloginfo('url')) . '>';
			    $verification_link = admin_url('admin-ajax.php') .
    				'?action=subscribe_newsletter&hash=' .
			        $hash .
    				'&email=' . $email .
    				'&tag=' . $tag;

			    $content = str_replace(
    				'[verification_link]',
    				preg_replace('@http(s)?://@', '', $verification_link),
    				$content
			    );

			    $mail_alternatives[$subject] = strip_tags($content);
			    $res2 = wp_mail( $email, $subject, $content, $headers);
			    setcookie('newsletter_sign', time(), time()+(60*15));
			}
			die();
		    }
		} else {
		    $res = $this->wp_action_to_sm($user, 'VISIT', 'Newsletter', 'Subskrypcja newslettera', $tags ,NULL, $contactDetail);
		    echo json_encode(['result' => $res, 'save' => 1]);
		    setcookie('newsletter_sign', time(), time()+(60*15));
		    die();
		}
	    }
	}

	/**
	 * Subscribe action
	 *
	 * @since    1.0.0
	 */

	public function subscribe_newsletter() {
	    $hash = $_REQUEST['hash']; 
	    $tag = $_REQUEST['tag']; 
	    $email = $_REQUEST['email'];
	    $valid = base64_encode(sha1(md5($tag).'MAILING'.md5($email)));
	    if ($valid != $hash) {
		Header('Location: https://magazynpismo.pl');
		die();
	    }


		$tags = [$tag . '-active'];
		$res = $this->wp_action_to_sm($email, 'VISIT', 'Newsletter', 'Subskrypcja newslettera - aktywacja', $tags);
		Header('Location: https://magazynpismo.pl/dziekujemy-za-zapis/');

	    die();
	}

	/**
	 * Add action
	 *
	 * @since    1.0.0
	 */
	public function wp_action_to_sm($user, $action = 'LOGIN', $location = 'Strona', $detail = '', $tags = null, $removetags = null, $contactDetails = null) {
	    global $wpdb;

	    $eventId = 0;

	    if (!$user) {
		return false;
	    }

	    $conn = new Smmp_Connection();
	    $data = [];
	    $data['contactEvent'] = [];
	    $data['contact'] = [];

	    $data['contactEvent']['date'] = time() * 1000;
	    $data['contactEvent']['contactExtEventType'] = $action;
	    $data['contactEvent']['shopDomain'] = 'magazynpismo.pl';
	    $data['contactEvent']['location'] = $location;
	    $data['contactEvent']['detail1'] = $detail;
	    $data['owner'] = $this->owner;

	    if (!is_null($contactDetails))
		$data['properties'] = $contactDetails;

	    if (is_object($user)) {
		$data['email'] = $user->user_email;
	    } else {
		if (is_string($user)) {
    		    $data['email'] = $user;
		} else {
		    return false;
		}
	    }

	    $resultA = $conn->action($data);

	    $data['contact']['email'] = $data['email'];

	    if (is_array($tags)) {
		$data['tags'] = $tags;
		unset($data['contactEvent']);
		$resultU = $conn->user($data, true);
	    }

	    if (is_array($removetags)) {
		$data['removeTags'] = $removetags;
		unset($data['contactEvent']);
		$resultU = $conn->user($data, true);
	    }

	    if (is_array($resultA)) {
		$eventId = $resultA['eventId'];
	    }

	    if (is_array($resultU)) {
		$eventId = $resultU['contactId'];
	    }

	    return $eventId;
	}

	/**
	 * Add transaction
	 *
	 * @since    1.0.0
	 */
	public function transaction_action_to_sm($pending_subscription, $subscription_plan, $type) {
	    global $wpdb;

	    $contactId = '';
	    $plan_wp_id = 0;
	    $transaction_date = '';
	    $user = get_user_by('ID', (int)$pending_subscription['user_id']);

	    if (array_key_exists('plan_wp_id', $pending_subscription) && $pending_subscription['plan_wp_id'] > 0) {
		$plan_wp_id = $pending_subscription['plan_wp_id'];
	    }

	    if (array_key_exists('transaction_date', $pending_subscription) && strlen($pending_subscription['transaction_date']) > 0) {
		$transaction_date = $pending_subscription['transaction_date'];
	    }

	    if (array_key_exists('transaction_id', $pending_subscription)) {
		$pending_subscription = $wpdb->get_row( $wpdb->prepare( "select * from `serviceuser_subscriptions_history` where user_id = %d and transaction_id = %d", $pending_subscription['user_id'], $pending_subscription['transaction_id'] ), ARRAY_A);
	    } else {
		$pending_subscription = $wpdb->get_row( $wpdb->prepare( "select * from `serviceuser_subscriptions_history` where id = %d", $pending_subscription['id'] ), ARRAY_A);
	    }

	    if ($plan_wp_id > 0) {
		$pending_subscription['plan_wp_id'] = $plan_wp_id;
	    }

	    if (strlen($transaction_date) > 0) {
		$pending_subscription['transaction_date'] = $transaction_date;
	    }

	    if (!$user) {
		return false;
	    }

	    $plan = get_post($pending_subscription['plan_wp_id']);
	    $plan_price = get_post_meta($pending_subscription['plan_wp_id'], 'price', true);
	    $plan_mit = get_post_meta($pending_subscription['plan_wp_id'], 'mit_chain', true);
	    $plan_name = $plan->post_title;

	    if ($plan_mit && strlen($plan_mit) > 1) {
		settype($plan_mit, 'DOUBLE');
		$plan_price = $plan_mit;
	    }

	    $conn = new Smmp_Connection();
	    $data = [];
	    $data['contactEvent'] = [];

	    if (strlen($pending_subscription['transaction_date'])) {
		$data['contactEvent']['date'] = strtotime($pending_subscription['transaction_date']) * 1000;
	    }
	    $data['contactEvent']['description'] = $plan->post_excerpt . ' ' . $pending_subscription['start_gmt_date'].' - '.$pending_subscription['end_gmt_date'];
	    $data['contactEvent']['value'] = $plan_price;
	    $data['contactEvent']['contactExtEventType'] = 'PURCHASE';
	    $data['contactEvent']['externalId'] = $pending_subscription['session_id'];
	    $data['contactEvent']['detail1'] = $plan_name;
	    $data['contactEvent']['detail2'] = $type;
	    $data['contactEvent']['location'] = 'Prenumerata';
	    $data['contactEvent']['shopDomain'] = 'magazynpismo.pl';
	    $data['contactEvent']['products'] = $pending_subscription['plan_wp_id'];
	    $data['owner'] = $this->owner;
	    $data['email'] = $user->user_email;
	    $data['forceOptIn'] = true;

	    $result = $conn->action($data);

	    if (is_array($result)) {
		$eventId = $result['contactId'];
	    }

	    setcookie('smevent', $eventId);
	}

	private function fix($str)
	{
	    $str = str_replace(chr(13),'', $str);
	    $str = str_replace(chr(10),'', $str);
	    return $str;
	}

	public function update_sm_clients()
	{
	    global $wpdb;
		$days = $_REQUEST['days'];

	    if ($days < 0) {
		$days = 1;
	    }

	    $transactions = $wpdb->get_results($wpdb->prepare( "SELECT id FROM serviceuser_subscriptions_history WHERE DATE(created_gmt_date) > (CURDATE() - INTERVAL %d DAY) ORDER BY id ASC", $days), ARRAY_A);
	    foreach ( $transactions as $row ) {
		$this->update_transaction_action_to_sm($row['id']);
	    }
	}

	/**
	    Generate product plans
	*/
	public function generate_plans()
	{
	    global $wpdb;
	    $in = fopen('/home/magazynpismo/public_html/sm/products.xml', 'w');
	    if ($in) {
		fwrite($in, '<?xml version="1.0" encoding="utf-8"?>'."\n");
		fwrite($in, '<offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">'."\n");
		fwrite($in, ''."\n");
		$plans = $wpdb->get_results($wpdb->prepare('SELECT wp_posts.id, post_title, post_name, pm.meta_value AS espago_plan, pm2.meta_value AS short_description, pm3.meta_value AS price
                FROM wp_posts
                LEFT JOIN wp_postmeta AS pm ON (pm.post_id = wp_posts.id AND pm.meta_key = \'espago_plan_id\')
                LEFT JOIN wp_postmeta AS pm2 ON (pm2.post_id = wp_posts.id AND pm2.meta_key = \'short_description\')
                LEFT JOIN wp_postmeta AS pm3 ON (pm3.post_id = wp_posts.id AND pm3.meta_key = \'price\')
                 WHERE post_type IN ("subscription_plan", "subscription_chain")
                 '), ARRAY_A);

		foreach ( $plans as $rows ) {
    		    fwrite($in,'<o id="' . $rows['id'] . '" url="https://magazynpismo.pl/Prenumeraty/'.$rows['post_name'].'" price="' . $rows['price'] . '" avail="1" weight="1" basket="1" stock="1000">'."\n");
    		    fwrite($in,'<cat><![CDATA[Prenumerata]]></cat>'."\n");
    		    fwrite($in,'<name>' . str_replace('&','&amp;', strip_tags($rows['post_title'])) . '</name>'."\n");
    		    fwrite($in, '<imgs>'."\n");
    		    fwrite($in,'<main url="https://magazynpismo.pl/wp-content-wlc-audio/themes/pismo-wp-theme/images/logo.svg" />'."\n");
    		    fwrite($in,'</imgs>'."\n");
    		    fwrite($in,'<desc>' . strip_tags($rows['short_description']) . '</desc>'."\n");
    		    fwrite($in,'<attrs>'."\n");
    		    fwrite($in,'<a name="PLAN_ID">' . $rows['espago_plan'] . '</a>'."\n");
    		    fwrite($in,'</attrs>'."\n");
    		    fwrite($in,'</o>'."\n");
		}

		fwrite($in, '</offers>'."\n");
		fclose($in);
	    }

	}

	/**
	    Refresh subscriptions
	*/
	public function validate_plans()
	{
	    global $wpdb;
	    $eventId = 0;

	    $conn = new Smmp_Connection();
	    $contacts = [];

	    $plans = $wpdb->get_results( "SELECT user_email,sd.paper_subscription_expires,subscription_expires_gmt_date, user_registered FROM wp_users AS wu
                JOIN serviceuser_data AS sd ON (sd.user_id = wu.ID)
		WHERE subscription_expires_gmt_date > DATE(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))" , ARRAY_A);

	    $idx = 1;
	    foreach ( $plans as $dbdata ) {
		$data = [];
		$dictionary = [];

		$t0 = strtotime($this->fix($dbdata["user_registered"]));
		$t1 = strtotime($this->fix($dbdata["subscription_expires_gmt_date"]));
	        $t2 = time();
		$pren = 0;
		if ($t1 > $t2 && $t1 > ($t0 + 100))
		    $pren = 1;

		$cont = [];
		$dictionary[] = ['name' => 'prenumerata', 'type' => 'NUMBER', 'value' => $pren];
		$cont['dictionaryProperties'] = $dictionary;
#		$data['owner'] = $this->owner;
		$cont['email'] = $dbdata['user_email'];
		$data['contact'] = $cont;

		$contacts[] = $data;

		if ($idx > 998) {
		    $result = $conn->users($contacts, $this->owner);
		    $contacts = [];
		    $idx = 1;
		}

		$idx++;

	    }

	    $result = $conn->users($contacts, $this->owner);

	    if (is_array($result)) {
		$eventId = $result['requestId'];
	    }

	    return $eventId;
	}

	public function parse_sm_request()
	{
	    $user = wp_get_current_user();

	    $url = $_SERVER['REQUEST_URI'];
	    if (strpos($url, '/panel/rezygnuj') !== false) {
		if (is_user_logged_in() && $user)
		    $this->wp_action_to_sm($user, 'VISIT', 'Magazyn Mój Profil', 'Próba rezygnacji');
	    }

	    if (strpos($url, '/panel/kontynuacja') !== false) {
		if (is_user_logged_in() && $user)
		    $this->wp_action_to_sm($user, 'VISIT', 'Magazyn Mój Profil', 'Wyjście z lejka utrzymaniowego bez rezygnacji');
	    }

	    if (strpos($url, '/panel/zmien-plan') !== false) {
		if (is_user_logged_in() && $user)
		    $this->wp_action_to_sm($user, 'VISIT', 'Magazyn Mój Profil', 'Zmiana planu w lejku utrzymaniowym');
	    }

	    if (strpos($url, '/panel/rezygnacja') !== false) {
		if (is_user_logged_in() && $user)
		    $this->wp_action_to_sm($user, 'VISIT', 'Magazyn Mój Profil', 'Rezygnacja z prenumeraty/subskrypcji');
	    }
	}
}
